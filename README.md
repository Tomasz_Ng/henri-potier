## Framework
- Nextjs

## Pre-requisite
- Nodejs v16.13.2
- npm v8.3.2

## Installation
- Clone project running "git clone git@bitbucket.org:Tomasz_Ng/henri-potier.git"
- Run "npm install" from root directory
- Run "npm dev" to run the development mode
- Run "npm test" to run unit-tests
 
## Missing features
- end-to-end tests

## Not finished (missing time)
- tests (actually 92% coverage)
- design (could be better)
