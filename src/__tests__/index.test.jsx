import React from "react";
import {render} from '@testing-library/react';
import Home from '../pages/index';
import products from './fixtures/products.json';
import { getServerSideProps } from "../pages/index";
import { Provider } from 'react-redux';
import store from '../redux/store';

describe('Home', () => {
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () =>
        Promise.resolve(products)
    })
  );

  it('should render properly', () => {
    let {container} = render(
      <Provider store={store}>
        <Home products={products}/>
      </Provider>
    )

    expect(container).toMatchSnapshot()
  });

  it("should call get books api call and return books successfully", async () => {
    const response = await getServerSideProps();
    expect(response).toEqual(
      expect.objectContaining({
        props: {
          products
        }
      })
    );
  });

  it("should call get books api call and fail", async () => {
    fetch.mockImplementationOnce(() => Promise.reject("API is down"));

    const response = await getServerSideProps({res: {}});

    expect(response).toEqual({props: {}});
    expect(fetch).toHaveBeenCalledWith(
      "https://henri-potier.techx.fr/books"
    );
  });
})
