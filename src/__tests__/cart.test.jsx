import React from "react";
import {render} from '@testing-library/react';
import Cart from '../pages/Cart';
import { Provider } from 'react-redux';
import store from '../redux/store';
import cart from "./fixtures/cart.json";
import offers from "./fixtures/offers.json";

describe('Cart', () => {
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () =>
        Promise.resolve(offers)
    })
  );

  it('should render properly', () => {
    let testState = store.getState();
    testState.cart = cart;

    let {container} = render(
      <Provider store={store}>
        <Cart />
      </Provider>
    )

    expect(container).toMatchSnapshot()
  })
})
