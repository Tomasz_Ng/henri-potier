import React from "react";
import { render, fireEvent } from '@testing-library/react';
import {within} from '@testing-library/dom';
import SearchBar from '../../pages/components/SearchBar';
import books from '../fixtures/products.json';
import { Provider } from 'react-redux';
import store from '../../redux/store';

describe('SearchBar', () => {
  it('should render properly', () => {
    let {container} = render(
      <Provider store={store}>
        <SearchBar products={books}/>
      </Provider>
    )

    expect(container).toMatchSnapshot()
  });

  it('should make a search by pressing enter', async () => {
    let {getByTestId} = render(<SearchBar products={books}/>)
    const autocomplete = getByTestId('autocomplete-search');
    const input = autocomplete.querySelector('input');

    fireEvent.change(input, { target: { value: 'some_value' } });
    fireEvent.keyDown(input, { key: 'Enter' });

    expect(input.value).toEqual('some_value');
  })

  it('should make a search typing and pressing enter', async () => {
    let {getByTestId} = render(<SearchBar products={books}/>)
    const autocomplete = getByTestId('autocomplete-search');
    const input = autocomplete.querySelector('input');

    fireEvent.change(input, { target: { value: 'some_value' } });
    fireEvent.keyDown(input, { key: 'Enter' });

    expect(input.value).toEqual('some_value');
  });

  it('should make a search by clicking on suggestion', async () => {
    let {getByTestId} = render(<SearchBar products={books}/>)
    const autocomplete = getByTestId('autocomplete-search');
    const input = autocomplete.querySelector('input');

    fireEvent.change(input, { target: { value: 'az' } });
    fireEvent.click(within(document.body).getByRole('presentation').querySelector('.MuiAutocomplete-option'));

    expect(input.value).toEqual('Henri Potier et le Prisonnier d\'Azkaban');
  })
})
