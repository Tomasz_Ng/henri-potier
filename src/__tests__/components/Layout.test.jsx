import React from "react";
import { render } from '@testing-library/react';
import Layout from '../../pages/components/Layout';
import { Provider } from 'react-redux';
import store from '../../redux/store';

describe('Layout', () => {
  it('should render properly', () => {
    let {container} = render(
      <Provider store={store}>
        <Layout />
      </Provider>
    )

    expect(container).toMatchSnapshot()
  })
})
