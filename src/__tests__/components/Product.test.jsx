import React from "react";
import {render, fireEvent} from '@testing-library/react';
import Product from '../../pages/components/Product';
import { Provider } from 'react-redux';
import products from '../fixtures/products.json';
import store from '../../redux/store';
import cart from "../fixtures/cart.json";

describe('Product', () => {
  it('should render properly', () => {
    let {container} = render(
      <Provider store={store}>
        <Product product={products[0]}/>
      </Provider>
    )

    expect(container).toMatchSnapshot()
  });

  it("should add single product to cart", async () => {
    let {container} = render(
      <Provider store={store}>
        <Product product={products[0]} isCart={false}/>
      </Provider>
    )

    let addToCartBtn = container.querySelector('.add-to-cart');

    fireEvent.click(addToCartBtn);

    expect(store.getState().cart.length).toEqual(1);
  });

  it("should add different products to cart", async () => {
    let {container} = render(
      <Provider store={store}>
        {products.map((book, index) => <Product key={index} product={book} isCart={false}/>)}
      </Provider>
    )

    let addToCartBtns = container.querySelectorAll('.add-to-cart');
    let firstAddToCartBtn = addToCartBtns[0];
    let secondAddToCartBtn = addToCartBtns[1];

    fireEvent.click(firstAddToCartBtn);
    fireEvent.click(secondAddToCartBtn);

    expect(store.getState().cart.length).toEqual(2);
  });

  it("should remove product from cart", async () => {
    let testState = store.getState();
    testState.cart = cart;

    let {container} = render(
      <Provider store={store}>
        {cart.map((book, index) => <Product key={index} product={book} isCart={true}/>)}
      </Provider>
    )

    let removeFromCartBtn = container.querySelector('.remove-from-cart');

    fireEvent.click(removeFromCartBtn);

    expect(store.getState().cart.length).toEqual(7);
  });
})
