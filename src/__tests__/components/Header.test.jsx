import React from "react";
import { render } from '@testing-library/react';
import Header from '../../pages/components/Header';
import { Provider } from 'react-redux';
import store from '../../redux/store';
import cart from "../fixtures/cart.json";

describe('Header', () => {
  it('should render properly', () => {
    let {container} = render(
      <Provider store={store}>
        <Header />
      </Provider>
    )

    expect(container).toMatchSnapshot()
  })

  it('should render properly with books in cart', () => {
    let testState = store.getState();
    testState.cart = [cart[0]];

    let {container} = render(
      <Provider store={store}>
        <Header />
      </Provider>
    )

    expect(container.querySelector('.quantity').textContent).toEqual(1);
  })
})
