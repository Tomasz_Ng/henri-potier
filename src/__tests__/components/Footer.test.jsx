import React from "react";
import { render } from '@testing-library/react';
import Footer from '../../pages/components/Footer';
import { Provider } from 'react-redux';
import store from '../../redux/store';

describe('Footer', () => {
  it('should render properly', () => {
    let {container} = render(
      <Provider store={store}>
        <Footer />
      </Provider>
    )

    expect(container).toMatchSnapshot()
  })
})
