import React from "react";

const Footer = () => {
  return (
    <footer>
      <p>© {new Date().getFullYear()} Tomasz Ngondo</p>
    </footer>
  )
}

export default Footer;
