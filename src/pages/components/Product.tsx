import React from "react";
import Image from "next/image";
import {Button} from "@mui/material";
import {RemoveShoppingCart, AddShoppingCart} from "@mui/icons-material";
import {addToCart, removeFromCart} from "../../redux/cart.slice";
import {useDispatch} from "react-redux";

const Product = (props: any) => {
  const dispatch = useDispatch();

  return (
    <figure className="product">
      <h3>{props.product.title}</h3>

      <div className="image-container">
        <div className="price">{props.product.price}€</div>
        <Image
          priority={true}
          src={props.product.cover}
          alt={props.product.title}
          width={props.isCart ? 300 : 600}
          height={props.isCart ? 400 : 800}/>
      </div>

      <figcaption>
        {props.isCart ? `Quantité: ${props.product.quantity}` : props.product.synopsis.flat()[0] }
      </figcaption>

      <div className="btns-container">
        {props.isCart ?
          <Button
            className="remove-from-cart"
            variant="outlined"
            endIcon={<RemoveShoppingCart />}
            onClick={() => dispatch(removeFromCart(props.product))}>
              Retirer
          </Button> : null}
          <Button
            className="add-to-cart"
            variant="outlined"
            endIcon={<AddShoppingCart />}
            onClick={() => dispatch(addToCart(props.product))}>
              Ajouter
          </Button>
      </div>
    </figure>
  )
}

export default Product;
