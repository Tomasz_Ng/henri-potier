import React from "react";
import Link from "next/link";
import { useSelector } from "react-redux";
import {Book} from "../../global/types";

const Header = () => {
  const cart = useSelector((state: any) => state.cart);
  const quantity = cart.map((item: Book) => item.quantity).reduce((a: number, b: number) => a + b, 0) || 0;

  return (
    <header>
      <nav>
        <h3><Link href="/">La bibliothèque d'Henri Potier</Link></h3>

        <ul>
          <li><Link href="/">Boutique</Link></li>
          <li className="cart-link">
            <Link href="/cart">Panier</Link>
            <span className="quantity">{quantity}</span>
          </li>
        </ul>
      </nav>
    </header>
  )
}

export default Header;
