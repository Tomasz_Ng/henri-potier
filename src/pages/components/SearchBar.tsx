import React from "react";
import { useRouter } from 'next/router';
import { Autocomplete, TextField } from '@mui/material';
import {Book} from "../../global/types";

export const SearchBar = (props: any) => {
  const router = useRouter();

  const handleOnChange = (event: any) => {
    const [url] = router && router.asPath.split('?') || "/";
    let searchQuery = "";

    if (event.key === "Enter") {
      searchQuery = event.target.value;
    } else if (event.target.dataset.optionIndex) {
      searchQuery = event.target.firstChild.data;
    }

    router && router.push(searchQuery ? `${url}?q=${searchQuery}` : `${url}`);
  }

  return (
    <section className="search">
      <Autocomplete
        freeSolo
        data-testid='autocomplete-search'
        className="search-bar"
        options={props.products.map((product: Book, index: number) => ({label: product.title, id: index}))}
        sx={{ minWidth: 300, width: "50vw" }}
        onChange={(event: any) => handleOnChange(event)}
        renderInput={(params) => (
          <TextField
            {...params}
            label="Rechercher un livre ..."
            InputProps={{
              ...params.InputProps
            }}
          />
        )}
      />

      {router && router.query && router.query.q ? <p>({props.products.length}) résultat(s) trouvé(s)</p> : null}
    </section>
  )
}

export default SearchBar;
