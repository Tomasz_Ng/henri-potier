import React, {useEffect, useState} from "react";
import type { NextPage } from 'next';
import {useSelector} from "react-redux";
import {Book, Offer} from "../global/types";
import Product from "./components/Product";

const Cart: NextPage = (props: any) => {
  const cart = useSelector((state: any) => state.cart);
  const isbns = cart.map((item: Book) => item.isbn);
  const cartPrice = cart.map((item: Book) => item.price).reduce((a: number, b: number) => a + b, 0);
  const [bestOffer, setBestOffer] = useState(cartPrice);
  const [isLoadingOffers, setLoadingOffers] = useState(false);

  useEffect(() => {
    if (cart.length > 0) {
      setLoadingOffers(true);
      fetch(`https://henri-potier.techx.fr/books/${isbns}/commercialOffers`)
        .then((res) => res.json())
        .then((res) => {
          let commercialOffer = handleCommercialOffers(res.offers, cartPrice);
          setBestOffer(commercialOffer);
          setLoadingOffers(false);
        })
    }
  }, [cart]);

  const handleCommercialOffers = (offers: Offer[], cartPrice: number) => {
    let commercialOffers: number[] = [];

    offers.map((offer: Offer) => {
      if (offer.type === "percentage") {
        commercialOffers.push(cartPrice - (cartPrice * (offer.value / 100)));
      } else if (offer.type === "minus") {
        commercialOffers.push(cartPrice - offer.value);
      } else {
        let sliceCount = Math.trunc(cartPrice / offer.sliceValue);
        commercialOffers.push(sliceCount > 0 ? cartPrice - (sliceCount * offer.value) : cartPrice);
      }
    });

    return Math.min(...commercialOffers)
  }

  return (
    <section className="cart">
      <aside className="cart-content">
        {cart.map((item: Book, index: number) => {
          return <Product key={index} product={item} isCart={true}/>
        })}
      </aside>

      <aside className="cart-pricing">
        <h3>Panier</h3>
        <p>Prix total: {bestOffer} €</p>
      </aside>
    </section>
  )
}

export default Cart;
