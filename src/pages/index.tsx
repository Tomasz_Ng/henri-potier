import React from "react";
import type { NextPage, GetServerSideProps } from 'next';
import { SearchBar } from './components/SearchBar';
import { useRouter } from 'next/router';
import type {Book} from "../global/types";
import Product from './components/Product';

export const getServerSideProps: GetServerSideProps = async (props) => {
  try {
    const response = await fetch(`https://henri-potier.techx.fr/books`);
    let products = await response.json();

    return {
      props: {
        products
      }
    };
  } catch {
    props.res.statusCode = 500;

    return {
      props: {},
    };
  }
}

const Home: NextPage<{ products: Book[] }> = (props) => {
  const router = useRouter();
  const searchQuery = router && router.query.q!;
  let products = props.products;

  if (searchQuery && Object.keys(searchQuery).length > 0) {
    products = products.filter((book: Book) => book.title.toLowerCase().includes(searchQuery.toString().toLowerCase())) || [];
  }

  return (
    <>
      <SearchBar products={products}/>
      <section className="products">
        {products.map((product: Book, index: number) => {
          return <Product product={product} key={index} isCart={false}/>
        })}
      </section>
    </>
  )
}

export default Home;
