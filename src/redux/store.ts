import { configureStore } from '@reduxjs/toolkit';
import { cartReducer } from './cart.slice';
import {combineReducers} from "redux";
import { persistReducer} from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const reducer = combineReducers({
  cart: cartReducer,
});

const persistConfig = {
  key: 'root',
  storage,
}

const persistedReducer = persistReducer(persistConfig, reducer)

const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false, // redux-persist
    })
});

export default store;
