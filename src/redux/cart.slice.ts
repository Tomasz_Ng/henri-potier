import { createSlice } from '@reduxjs/toolkit';
import {Book} from "../global/types";

const cartSlice = createSlice({
  name: 'cart',
  initialState: [],
  reducers: {
    addToCart: (state: Book[], action) => {
      const itemExists = state.find((item) => item.isbn === action.payload.isbn);

      if (itemExists) {
        itemExists.quantity++;
      } else {
        state.push({ ...action.payload, quantity: 1 });
      }
    },
    removeFromCart: (state: Book[], action) => {
      const itemExists = state.find((item) => item.isbn === action.payload.isbn);
      const index = state.findIndex((item) => item.isbn === action.payload.isbn);

      if (itemExists) {
        if (itemExists.quantity > 1) {
          itemExists.quantity--;
        } else {
          state.splice(index, 1);
        }
      }
    },
  },
});

export const cartReducer = cartSlice.reducer;

export const {
  addToCart,
  removeFromCart,
} = cartSlice.actions;
