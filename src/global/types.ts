export interface Book {
  title: string;
  price: number;
  synopsis: string[];
  cover: string;
  isbn: string;
  quantity: number;
}

export interface Offer {
  type: string;
  value: number;
  sliceValue: number;
}
